import UIKit

final class QuestionList: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tag = ""
    private let dataManager = UserDefaultsProvider()
    private let networking = Networking()
    private var datas:[Item] = []
    private let refresh: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        readItemsInUD()
        showObjcFirstContent()
        tableView.refreshControl = refresh
    }
    
    func readItemsInUD() {
        if let items = dataManager.read(key: "Objective-C") {
            self.datas = items
            self.tableView.reloadData()
        }
    }
    
    func showObjcFirstContent() {
        networking.networkQuest(fortag: "Objective-C", completion: { [weak self] items in
            DispatchQueue.main.async {
                guard let items = items else { return }
                self?.dataManager.save(object: items, key: "Objective-C")
                self?.datas = items
                self?.tableView.reloadData()
            }
        })
    }
    
    func networkRequest() {
        if let items = dataManager.read(key: tag) {
            self.datas = items
            self.tableView.reloadData()
            print("Все остальное")
        }
        
        networking.networkQuest(fortag: tag, completion: { [weak self] items in
            guard let items = items else { return }
            self?.datas = items
            self?.dataManager.save(object: items, key: self?.tag ?? "Objective-C")
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.tableView.refreshControl?.endRefreshing()
            }
        })
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        networkRequest()
    }
}

extension QuestionList: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = datas[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CustomTableViewCell else {fatalError("Wrong cell class")}
        cell.setDatas(celldata: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Передача id
        let id = self.datas[indexPath.row].questId
        guard let answerVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AnswerViewController") as? AnswerViewController else {fatalError("Wrong cell class")}
        answerVC.questionId = id.description
        answerVC.answerDatas.append(self.datas[indexPath.row])
        
        // Условие для отображения ошибки, если ответов = 0
        if  self.datas[indexPath.row].answerCount == 0 {
            let alert = UIAlertController(title: "Warning", message: "No answers", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            present(alert, animated: true, completion: nil)
            
        } else {
            self.navigationController?.pushViewController(answerVC, animated: true)
        }
    }
}
