import Foundation

final class UserDefaultsProvider {
    // Метод для кодирования данных
    func save(object: [Item], key: String) {
        let encodedData = try? JSONEncoder().encode(object)
        UserDefaults.standard.set(encodedData, forKey: key)
    }
    
    // Метод для чтения данных
    func read(key: String) -> [Item]? {
        guard let data = UserDefaults.standard.data(forKey: key) else { return nil }
        let elements = try? JSONDecoder().decode(ItemList.self, from: data)
        return elements
    }
}
