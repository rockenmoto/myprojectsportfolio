//
//  ViewController.swift
//  TASK4
//
//  Created by Kirill Belov on 22.10.2020.
//

import UIKit

final class ViewController: UIViewController {
    
    @IBOutlet private weak var myTextField: UITextField!
    @IBOutlet private weak var errorLabel: UILabel!
    
    private var loginValidation = LoginValidation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboard()
    }
    
    func hideKeyboard() {
        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hKeyboard))
        self.view?.addGestureRecognizer(hideKeyboardGesture)
    }
    
    @objc private func hKeyboard() {
        self.view?.endEditing(true)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let text = self.loginValidation.loginValidation(textField.text)
        guard let textfield = textField.text else { return }
        
        if textfield.isEmpty {
            self.errorLabel.isHidden = true
        } else if text {
            self.errorLabel.isHidden = false
            self.errorLabel.text = "Хорошеечно"
            self.errorLabel.backgroundColor = .green
            
        } else {
            self.errorLabel.isHidden = false
            self.errorLabel.text = "Что - то пошло не так !!! \nЛогин и почта должны быть на английском языке !!!"
            self.errorLabel.backgroundColor = .red
        }
    }
}

