//
//  Networking.swift
//  TASK3
//
//  Created by Kirill Belov on 19.10.2020.
//

import Foundation
import Alamofire

protocol NetworkingInput {
    func loadUserRepos(_ api: APIProvider, completion: @escaping ([ArrayModelElement]?, String?) -> Void)
}

final class Networking: NetworkingInput {
    func loadUserRepos(_ api: APIProvider, completion: @escaping ([ArrayModelElement]?, String?) -> Void) {
        AF.request(api.url).responseJSON(queue: .global()) { response in
            switch response.result {
            case .success:
                if let data = response.data,
                   let users = try? JSONDecoder().decode([ArrayModelElement].self, from: data) {
                    completion(users, nil)
                }
            case .failure(let value):
                completion(nil, value.localizedDescription)
            }
        }
    }
}

