import UIKit

final class MenuViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private let menuList = ["Objective_C",
                            "IOS",
                            "Xcode",
                            "Cocoa_touch",
                            "Iphone"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuLabel", for: indexPath)
        cell.textLabel?.text = menuList[indexPath.row]
        cell.textLabel?.numberOfLines = 0
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let container = self.parent as? ContainerViewController {
            let tag = self.menuList[indexPath.row]
            container.sendTag(tag: tag)
        }
    }
}
