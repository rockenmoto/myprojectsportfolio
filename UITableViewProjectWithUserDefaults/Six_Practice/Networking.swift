import Foundation

final class Networking {
    
    func networkQuest(fortag tag: String, completion: @escaping ([Item]?) -> Void) {
        let urlString = "https://api.stackexchange.com/2.2/questions?order=desc&sort=activity&tagged=\(tag)&site=stackoverflow"
        guard let url = URL(string: urlString) else { return }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { data, response, error in
            if let data = data, let questModel = self.parseJSON(withData: data) {
                completion(questModel)
            } else {
                completion(nil)
            }
        }
        task.resume()
    }
    
    func parseJSON(withData data: Data) -> [Item]? {
        let decoder = JSONDecoder()
        do {
            let questionModelData = try decoder.decode(QuestionModelData.self, from: data)
            
            return questionModelData.items
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
    
    // Метод для Таблицы ответа
    func networkAnswer(forId id: String, completion: @escaping ([ItemItem]?) -> Void) {
        let urlString = "https://api.stackexchange.com/2.2/questions/\(id)/answers?order=desc&sort=activity&site=stackoverflow&filter=!4-8tq2HgCr_we7f(X"
        guard let url = URL(string: urlString) else { return }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { data, response, error in
            if let data = data, let answerModel = self.parseJSONAnswer(withData: data) {
                completion(answerModel)
            } else {
                completion(nil)
            }
        }
        task.resume()
    }
    
    func parseJSONAnswer(withData data: Data) -> [ItemItem]? {
        let decoder = JSONDecoder()
        do {
            let answerModelData = try decoder.decode(Answer.self, from: data)            
            return answerModelData.items
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
