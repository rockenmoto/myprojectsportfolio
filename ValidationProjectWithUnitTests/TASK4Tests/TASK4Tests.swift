//
//  TASK4Tests.swift
//  TASK4Tests
//
//  Created by Kirill Belov on 25.10.2020.
//

import XCTest
@testable import TASK4

class TASK4Tests: XCTestCase, UITextFieldDelegate {
    var loginValidation: LoginValidation!
    
    override func setUpWithError() throws {
        self.loginValidation = LoginValidation()
    }
    
    override func tearDownWithError() throws {
        self.loginValidation = nil
    }
    
    
    //Выполнение проверки на минимальную длину логина - 3 символа и максимальную - 32
    func testLoginLength() throws {
        let negativeExpectations = [
            self.loginValidation.loginValidation("l"), // 1
            self.loginValidation.loginValidation("ll"), // 2
            self.loginValidation.loginValidation("lllllllllllllllllllllllllllllllll") // 33
        ]
        
        let positiveExpectations = [
            self.loginValidation.loginValidation("lll"), // 3
            self.loginValidation.loginValidation("llllllllllllllllllllllllllllllll") // 32
        ]
        
        negativeExpectations.forEach { result in
            XCTAssertFalse(result)
        }
        
        positiveExpectations.forEach { result in
            XCTAssertTrue(result)
        }
    }
    
    
    //Выполнение проверки "Логин может состоять из латинских букв, цифр, минуса и точки."
    //Логин не может начинаться на цифру, точку, минус.
    func testLoginInvalidStartCharacters() throws {
        let negativeExpectations = [
            self.loginValidation.loginValidation("1ll"),
            self.loginValidation.loginValidation(".ll"),
            self.loginValidation.loginValidation("-ll"),
            self.loginValidation.loginValidation(" -ll") // c пробелом
        ]
        
        let positiveExpectations = [
            self.loginValidation.loginValidation("l1."),
            self.loginValidation.loginValidation("l1-.")
        ]
        
        negativeExpectations.forEach { result in
            XCTAssertFalse(result)
        }
        
        positiveExpectations.forEach { result in
            XCTAssertTrue(result)
        }
    }
    
    
    // В данной проверке русские символы не проходят валидацию для Логина, принимаются только латинкие символы
    func testLogin() throws {
        let negativeExpectations = [
            self.loginValidation.loginValidation("Миролюб"),
            self.loginValidation.loginValidation("VasiliyТеркин"),
            self.loginValidation.loginValidation("Vasiliy Terkin"),
        ]
        
        let positiveExpectations = [
            self.loginValidation.loginValidation("John"),
            self.loginValidation.loginValidation("Mike23")
        ]
        
        negativeExpectations.forEach { result in
            XCTAssertFalse(result)
        }
        
        positiveExpectations.forEach { result in
            XCTAssertTrue(result)
        }
    }
    
    
    //Проверка введенный почты как логина
    // Имя может быть как одними цифрами (например номер телефона), так и согласно общим требованиям
    // Добавил возможность писать почту с русским доменном
    func testLoginAsEmail() throws {
        let negativeExpectations = [
            self.loginValidation.loginValidation("m@mm.m"),
            self.loginValidation.loginValidation("-m@mm.mm"),
            self.loginValidation.loginValidation(".m@mm.mm"),
            self.loginValidation.loginValidation("@m@mm.mm"),
            self.loginValidation.loginValidation(" .mm@mm.m") // c пробелом
        ]
        
        let positiveExpectations = [
            self.loginValidation.loginValidation("1234567@mmm.mm"),
            self.loginValidation.loginValidation("mm23@mm.mm"),
            self.loginValidation.loginValidation("mM-mm.mm@mm333.mm"),
            self.loginValidation.loginValidation("mmm@москва.рф"),
            self.loginValidation.loginValidation("mmm@mmmmmmmmmmmmmmmmmmmmmmmm.mm")

        ]
        
        negativeExpectations.forEach { result in
            XCTAssertFalse(result)
        }
        
        positiveExpectations.forEach { result in
            XCTAssertTrue(result)
        }
    }
}
