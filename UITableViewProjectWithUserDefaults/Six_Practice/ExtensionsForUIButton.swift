import UIKit

extension UIButton {
    func pulsate(){
        let pulse = CASpringAnimation(keyPath: "opacity")
        layer.add(pulse, forKey: nil)
        pulse.duration = 0.2
        pulse.fromValue = 1
        pulse.toValue = 0.2
        pulse.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        pulse.autoreverses = true
        pulse.repeatCount = 0.0
    }
}
