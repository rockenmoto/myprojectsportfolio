//
//  ArrayModel.swift
//  TASK3
//
//  Created by Kirill Belov on 19.10.2020.
//

import Foundation

struct ArrayModelElement: Decodable {
    let name: String
}
