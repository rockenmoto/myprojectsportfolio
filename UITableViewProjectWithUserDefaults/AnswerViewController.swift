import UIKit

final class AnswerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var answerDatas: [Codable] = []
    var questionId = ""
    private let networking = Networking()
    private let refresh: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnswers()
        tableView.refreshControl = refresh
    }
    
    func showAnswers() {
        networking.networkAnswer(forId: questionId, completion: { [weak self] items in
            guard let items = items else { return }
            self?.answerDatas.append(contentsOf: items)
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        })
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        networking.networkAnswer(forId: questionId, completion: { [weak self] items in
            guard let items = items else { return }
            self?.answerDatas.append(contentsOf: items)
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.tableView.refreshControl?.endRefreshing()
            }
        })
    }
}

extension AnswerViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answerDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let data = answerDatas[indexPath.row] as? ItemItem {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as? AnswerTableViewCell else {fatalError("Wrong cell class")}
            cell.setAnswer(celldata: data)
            
            // Получение Cherkmarks
            if data.isAccepted == true {
                cell.checkMark.isHidden = false
            } else {
                cell.checkMark.isHidden = true
            }
            return cell
            
        } else {
            guard let data = answerDatas[indexPath.row] as? Item else {fatalError("Wrong Data")}
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CustomTableViewCell else {fatalError("Wrong cell class")}
            cell.setDatas(celldata: data)
            return cell
        }
    }
}
