import Foundation
import UIKit

typealias ItemList = [Item]

struct QuestionModelData: Codable {
    let items: [Item]
    
    enum CodingKeys: String, CodingKey {
        case items
    }
}

struct Item: Codable {
    let owner: Owner
    let answerCount: Int
    let questId: Int
    let creationDate: Int
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case owner
        case answerCount = "answer_count"
        case creationDate = "creation_date"
        case title
        case questId = "question_id"
    }
}

struct Owner: Codable {
    let displayName: String
    
    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
    }
}
