import UIKit

final class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var authorOfLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var questionLabel: UILabel!
    @IBOutlet private weak var textOfLabel: UILabel!
    
    // Тут присваиваю значения из АПИ к аутлетам
    func setDatas(celldata: Item) {
        self.authorOfLabel.text = celldata.owner.displayName
        self.questionLabel.text = celldata.answerCount == 0 ? "No answer" : "Answers: \(celldata.answerCount)"
        self.textOfLabel.text = celldata.title
        
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let date = Date(timeIntervalSince1970: TimeInterval(celldata.creationDate))
        self.dateLabel.text = df.string(from: date)
    }
}
