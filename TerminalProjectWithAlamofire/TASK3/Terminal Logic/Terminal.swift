//
//  Terminal.swift
//  TASK3
//
//  Created by Kirill Belov on 13.10.2020.
//

import Foundation

final class Terminal {
    
    private let consoleIO = ConsoleIO()
    private var arrayModel: [ArrayModelElement] = []
    private let networking: NetworkingInput
    
    init(networking: NetworkingInput) {
        self.networking = networking
    }
    
    func showRepoList() {
        if arrayModel.isEmpty {
            print("\nНичего не найдено...")
        } else {
            consoleIO.writeMessage("")
            print("Список репозиториев пользователя из Github:")
            for (index, value) in arrayModel.enumerated() {
                print("\n\(index + 1): \(value.name)")
            }
        }
    }
    
    func interactiveMode() {
        consoleIO.writeMessage("=== Добро пожаловать в библиотеку репозиториев из Github ===")
        consoleIO.writeMessage("\nВведите имя пользователя:")
        var shouldQuit = false
        while !shouldQuit {
            let option = consoleIO.getInput()
            if option == "q" {
                shouldQuit = true
            } else {
                self.getUserRepos(option)
            }
        }
    }
    
    func getUserRepos(_ userName: String) {
        networking.loadUserRepos(.repos(name: userName)) { [weak self] (items, error) in
            if let error = error {
                self?.consoleIO.writeMessage("\nError: \(error)")
            }
            guard let items = items else { return }
            self?.arrayModel = items
            self?.showRepoList()
            self?.consoleIO.writeMessage("\nВведите имя пользователя:")
        }
    }
}
