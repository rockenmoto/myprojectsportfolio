import Foundation

struct Answer: Codable {
    let items: [ItemItem]
    
    enum CodingKeys: String, CodingKey {
        case items
    }
}

struct ItemItem: Codable {
    let owner: Owners
    let isAccepted: Bool
    let score: Int
    let body: String
    let creationDate: Int
    
    enum CodingKeys: String, CodingKey {
        case owner
        case isAccepted = "is_accepted"
        case score
        case body
        case creationDate = "creation_date"
    }
}

struct Owners: Codable {
    let displayName: String
    
    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
    }
}
