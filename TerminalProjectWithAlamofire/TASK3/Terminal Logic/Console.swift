//
//  Console.swift
//  TASK3
//
//  Created by Kirill Belov on 13.10.2020.
//

import Foundation

class ConsoleIO {
    func writeMessage(_ message: String) {
        print("\(message)")
      }
    
    func getInput() -> String {
      let keyboard = FileHandle.standardInput
      let inputData = keyboard.availableData
      let strData = String(data: inputData, encoding: String.Encoding.utf8)
      return (strData?.trimmingCharacters(in: CharacterSet.newlines))!
    }
}
