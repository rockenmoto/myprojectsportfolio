import UIKit

final class AnswerTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var authorAnswer: UILabel!
    @IBOutlet private weak var dateAnswer: UILabel!
    @IBOutlet private weak var numberAnswer: UILabel!
    @IBOutlet private weak var textAnswer: UILabel!
    @IBOutlet weak var checkMark: UIImageView!
    
    func setAnswer(celldata: ItemItem) {
        authorAnswer.text = celldata.owner.displayName
        numberAnswer.text = "Scores: \(celldata.score)"
        textAnswer.text = celldata.body.description.htmlToString
                
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let date = Date(timeIntervalSince1970: TimeInterval(celldata.creationDate))
        self.dateAnswer.text = df.string(from: date)
    }
}
