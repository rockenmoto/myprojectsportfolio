//
//  ApiProvider.swift
//  TASK3
//
//  Created by Kirill Rockenmota on 11.11.2020.
//

import Foundation

enum APIProvider {
    case repos(name: String)
}

extension APIProvider {
    private var baseURL: URL  {
        return URL(string: "https://api.github.com/")!
    }
    
    var url: URL {
        switch self {
        case .repos(let name):
            return baseURL.appendingPathComponent("users/\(name)/repos")
        }
    }
}
