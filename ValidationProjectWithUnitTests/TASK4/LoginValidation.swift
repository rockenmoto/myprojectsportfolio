//
//  LoginValidation.swift
//  TASK4
//
//  Created by Kirill Belov on 11.11.2020.
//

import Foundation

protocol LoginValidationProvider {
    func loginValidation(_ text: String?) -> Bool
    func validation(regex: RegexValidate, text: String) -> Bool
}

enum RegexValidate: String {
    case login = "^[a-zA-Z][a-zA-Z0-9.-]{2,31}$"
    case email = "^[0-9a-zA-Z][A-Z0-9a-z.-]+@[A-Za-z0-9-а-яёa-z]+\\.[а-яёa-z]{2,}$"
}

final class LoginValidation: LoginValidationProvider {
    func validation(regex: RegexValidate, text: String) -> Bool {
        let range = text.range(of: regex.rawValue, options: .regularExpression)
        return range != nil
    }
    
    func loginValidation(_ text: String?) -> Bool {
        var result = false
        
        guard let text = text else { return result }
        
        if text.contains("@"), (3...34).contains(text.count) {
            result = self.validation(regex: .email, text: text)
        } else {
            result = self.validation(regex: .login, text: text)
        }
        return result
    }
}
