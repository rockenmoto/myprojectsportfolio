import UIKit

final class ContainerViewController: UIViewController {
    
    private var questViewController: QuestionList!
    private var menuViewController: MenuViewController!
    private var isMove = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureQuestViewController()
        showShadowForQuestViewController(true)
        swipeGesture()
    }
    
    func configureQuestViewController() {
        self.questViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "QuestionList") as? QuestionList
        view.addSubview(questViewController.view)
        addChild(questViewController)
        questViewController.didMove(toParent: self)
    }
    
    func configureMenuViewController() {
        if menuViewController == nil {
            self.menuViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MenuViewController") as? MenuViewController
            view.insertSubview(menuViewController.view, at: 0)
            addChild(menuViewController)
        }
    }
    
    func showMenuViewController(shouldMove: Bool) {
        if shouldMove {
            // Отображаем меню
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: {
                            self.questViewController.view.frame.origin.x = self.questViewController.view.frame.width - 250
                           }) { (finished) in }
            questViewController.tableView.allowsSelection = false
            questViewController.tableView.isScrollEnabled = false
        } else {
            // Скрываем меню
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: {
                            self.questViewController.view.frame.origin.x = 0
                           }) { (finished) in }
            questViewController.tableView.allowsSelection = true
            questViewController.tableView.isScrollEnabled = true
        }
    }
    
    @IBAction func openMenu(_ sender: UIButton) {
        sender.pulsate()
        toggleMenu()
    }
    
    func showShadowForQuestViewController ( _ shouldShowShadow: Bool) {
        if shouldShowShadow {
            questViewController.view.layer.shadowOpacity = 0.8
        } else {
            questViewController.view.layer.shadowOpacity = 0.0
        }
    }
    
    func toggleMenu() {
        configureMenuViewController()
        isMove = !isMove
        showMenuViewController(shouldMove: isMove)
    }
    
    func sendTag(tag: String) {
        self.questViewController.tag = tag
        self.questViewController.networkRequest()
        configureMenuViewController()
        isMove = !isMove
        showMenuViewController(shouldMove: isMove)
        self.navigationItem.title = tag
    }
}

// MARK: Gesture recognizer
extension ContainerViewController: UIGestureRecognizerDelegate {
    
    func swipeGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self,action: #selector(handleSwipes(gester:)))
        swipeRight.direction = .right
        questViewController.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(
            target: self,
            action: #selector(handleSwipes(gester:)))
        swipeLeft.direction = .left
        questViewController.view.addGestureRecognizer(swipeLeft)
    }
    
    @objc func handleSwipes(gester: UISwipeGestureRecognizer) {
        switch gester.direction {
        case .right:
            toggleMenu()
            
        case .left:
            toggleMenu()
            
        default:
            break
        }
    }
}
